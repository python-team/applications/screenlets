# Russian translation for screenlets
# Copyright (c) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same license as the screenlets package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: screenlets\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2008-02-13 19:35+0200\n"
"PO-Revision-Date: 2008-04-14 09:25+0000\n"
"Last-Translator: Maxim Prokopyev <xjazz@yandex.ru>\n"
"Language-Team: Russian <ru@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2008-05-04 15:36+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#: XmlMenu.py:48
msgid "Error while creating image from file: %s"
msgstr "Ошибка во время создания изображения из файла: %s"

#: XmlMenu.py:64
msgid "Error: file %s not found."
msgstr "Ошибка: файл %s не найден."

#: XmlMenu.py:185
msgid "XML-Error: %s"
msgstr "Ошибка XML: %s"

#: XmlMenu.py:231
msgid "Exception: %s"
msgstr "Исключение: %s"

#: XmlMenu.py:232
msgid "An error occured with desktop-file: %s"
msgstr "Произошла ошибка с desktop-файлом: %s"

#: XmlMenu.py:338
msgid "Error while creating menu."
msgstr "Ошибка во время создания меню"

#: __init__.py:154
msgid "Error while loading ScreenletTheme in: "
msgstr "Ошибка во время загрузки темы в: "

#: __init__.py:175
msgid "Override: "
msgstr "Перезапись: "

#: __init__.py:186
msgid "WARNING: Option '%s' not found or protected."
msgstr "Внимание: Параметр '%s' не найден или защищен."

#: __init__.py:411
msgid "theme.conf loaded: "
msgstr "theme.conf загружен: "

#: __init__.py:412
msgid "Name: "
msgstr "Имя: "

#: __init__.py:413
msgid "Author: "
msgstr "Автор: "

#: __init__.py:414
msgid "Version: "
msgstr "Версия: "

#: __init__.py:415
msgid "Info: "
msgstr "Сведения : "

#: __init__.py:417
msgid "Failed to load theme.conf"
msgstr "Не удалось загрузить файл theme.conf"

#: __init__.py:469
msgid "theme.conf found! Loading option-overrides."
msgstr "Найден theme.conf! Идет загрузка с перекрытием опций"

#: __init__.py:524
msgid "No name set for this Screenlet"
msgstr "У этого Скринлета нет имени"

#: __init__.py:526
msgid "No author defined for this Screenlet"
msgstr "Автор этого Скринлета не указан"

#: __init__.py:527
msgid "No info set for this Screenlet"
msgstr "Информация об этом Скринлете не указана"

#: __init__.py:626
msgid "The basic settings for this Screenlet-instance."
msgstr ""

#: __init__.py:638
msgid "The X-position of this Screenlet ..."
msgstr "X-координата для этого Скринлета ..."

#: __init__.py:638
msgid "X-Position"
msgstr "Х-координата"

#: __init__.py:641
msgid "The Y-position of this Screenlet ..."
msgstr "Y-координата для этого Скринлета ..."

#: __init__.py:641
msgid "Y-Position"
msgstr "Y-координата"

#: __init__.py:644
msgid "The width of this Screenlet ..."
msgstr "Ширина этого Скринлета ..."

#: __init__.py:644
msgid "Width"
msgstr "Ширина"

#: __init__.py:647
msgid "Height"
msgstr "Высота"

#: __init__.py:647
msgid "The height of this Screenlet ..."
msgstr "Высота этого Скринлета ..."

#: __init__.py:650
msgid "Scale"
msgstr "Масштаб"

#: __init__.py:650
msgid "The scale-factor of this Screenlet ..."
msgstr "Масштаб этого Скринлета ..."

#: __init__.py:653
msgid "Opacity"
msgstr "Прозрачность"

#: __init__.py:653
msgid "The opacity of the Screenlet window ..."
msgstr "Прозрачность окна скринлета ..."

#: __init__.py:656
msgid "Stick to Desktop"
msgstr "Прикрепить к Рабочему Столу"

#: __init__.py:657
msgid "Show this Screenlet on all workspaces ..."
msgstr "Показывать этот скринлет на всех рабочих местах"

#: __init__.py:659
msgid "Treat as Widget"
msgstr ""

#: __init__.py:660
msgid "Treat this Screenlet as a \"Widget\" ..."
msgstr ""

#: __init__.py:662
msgid "Lock position"
msgstr "Закрепить"

#: __init__.py:663
msgid "Stop the screenlet from being moved..."
msgstr ""

#: __init__.py:665
#: __init__.py:920
msgid "Keep above"
msgstr "Всегда наверху"

#: __init__.py:666
msgid "Keep this Screenlet above other windows ..."
msgstr "Закрепить Скринлет поверх остальных окон ..."

#: __init__.py:668
#: __init__.py:927
msgid "Keep below"
msgstr "Всегда снизу"

#: __init__.py:669
msgid "Keep this Screenlet below other windows ..."
msgstr "ЗакрепитьСкринлет под остальными окнами ..."

#: __init__.py:671
msgid "Draw button controls"
msgstr "Отображать управляющие кнопки"

#: __init__.py:672
msgid "Draw buttons in top right corner"
msgstr "Отображать кнопки в правом верхнем углу"

#: __init__.py:674
msgid "Skip Pager"
msgstr "Не отображать в переключателе раб. мест"

#: __init__.py:675
msgid "Set this Screenlet to show/hide in pagers ..."
msgstr ""
"Включить/выключить отображение Скринлета на переключателе рабочих мест"

#: __init__.py:677
msgid "Skip Taskbar"
msgstr "Не отображать в списке окон"

#: __init__.py:678
msgid "Set this Screenlet to show/hide in taskbars ..."
msgstr ""
"Включить/выключить отображение Скринлета в списке окон (на панели задач)"

#: __init__.py:681
msgid "Allow overriding Options"
msgstr "Разрешить изменение настроек"

#: __init__.py:682
msgid "Allow themes to override options in this screenlet ..."
msgstr "Разрешить темам изменять настройки этого Скринлета ..."

#: __init__.py:684
msgid "Ask on Override"
msgstr "Спрашивать при изменении"

#: __init__.py:685
msgid "Show a confirmation-dialog when a theme wants to override "
msgstr ""
"Показывать диалог подтверждения если тема хочет перезаписать настройки "

#: __init__.py:686
msgid "the current options of this Screenlet ..."
msgstr "текущие настройки этого Скринлета ..."

#: __init__.py:784
msgid "LOAD NEW THEME: "
msgstr "ЗАГРУЗИТЬ НОВУЮ ТЕМУ: "

#: __init__.py:785
msgid "FOUND: "
msgstr "НАЙДЕНО: "

#: __init__.py:863
msgid "Size"
msgstr "Размер"

#: __init__.py:878
msgid "Theme"
msgstr "Тема"

#: __init__.py:893
msgid "Window"
msgstr "Окно"

#: __init__.py:899
msgid "Lock"
msgstr "Блокировать"

#: __init__.py:906
msgid "Sticky"
msgstr "Прилепить"

#: __init__.py:913
msgid "Widget"
msgstr "Виджет"

#: __init__.py:936
msgid "Properties..."
msgstr "Свойства..."

#: __init__.py:940
msgid "Info..."
msgstr "Сведения..."

#: __init__.py:944
msgid "Delete Screenlet ..."
msgstr "Удалить Скринлет ..."

#: __init__.py:947
msgid "Quit this %s ..."
msgstr "Закрыть %s ..."

#: __init__.py:950
msgid "Quit all %ss ..."
msgstr "Закрыть все %ss ..."

#: __init__.py:1134
msgid "Error while loading theme: "
msgstr "Ошибка во время загрузки темы: "

#: __init__.py:1144
msgid ""
"This theme wants to override your settings for this Screenlet. Do you want "
"to allow that?"
msgstr "Данная Тема пытается изменить настройки Скринлета. Разрешить?"

#: __init__.py:1261
msgid "UPDATING SHAPE"
msgstr "ОБНОВЛЕНИЕ ФОРМЫ"

#: __init__.py:1347
msgid "To quit all %s's, use 'Quit' instead. "
msgstr "Чтобы закрыть все %s, используйте 'Закрыть'. "

#: __init__.py:1348
msgid "Really delete this %s and its settings?"
msgstr "действительно удалить %s и все его настройки?"

#: __init__.py:1534
#: __init__.py:1537
msgid "MOUSEWHEEL"
msgstr ""

#: __init__.py:1556
msgid "Compositing method changed to %s"
msgstr ""

#: __init__.py:1562
msgid "Warning - Buttons will not be shown until screenlet is restarted"
msgstr ""
"Внимание - Кнопки не будут отображаться, пока Скринлет не будет перезагружен"

#: __init__.py:1592
msgid "Cancel delete_event"
msgstr "Отменить delete_event"

#: __init__.py:1612
msgid "Start drag"
msgstr "Начать перетаскивание"

#: __init__.py:1621
msgid "End drag"
msgstr ""

#: __init__.py:1697
msgid "Quitting current screenlet instance"
msgstr ""

#: __init__.py:1714
msgid "Screenlet: Set theme %s"
msgstr "Screenlet: Назначить тему %s"

#: __init__.py:1723
msgid "Error: Cannot set missing or non-boolean value '"
msgstr ""

#: backend.py:33
msgid "GConf python module not found. GConf settings backend is disabled."
msgstr ""
"Модуль GConf для Python не найден. GConf не будет использоваться в качестве "
"хранилища настроек."

#: backend.py:72
msgid "GConfBackend: initializing"
msgstr "GConfBackend: инициализация"

#: backend.py:102
msgid "Saved option %s%s/%s = %s"
msgstr "Сохраненные параметры %s%s/%s = %s"

#: backend.py:135
msgid "Temporary file didn't exist - nothing to remove."
msgstr "Временный файл не существует - нечего удалять."

#: backend.py:137
msgid "CachingBackend: <#%s> removed!"
msgstr "CachingBackend: <#%s> удален!"

#: backend.py:176
msgid "CachingBackend: Loading instances from cache"
msgstr "CachingBackend: Загрузка значений из кэша."

#: backend.py:186
msgid "CachingBackend: Loading <%s>"
msgstr "CachingBackend: Загрузка <%s>"

#: backend.py:202
msgid "Error while loading options: %s"
msgstr "Ошибка во время загрузки параметров: %s"

#: backend.py:210
msgid "Queue-element <%s> not found (already removed?)!"
msgstr "Элемент очереди (Queue-element) <%s> не найден (удален?)!"

#: backend.py:214
msgid "CachingBackend: Saving <#%s> :) ..."
msgstr "CachingBackend: Сохраняется <#%s> :) ..."

#: backend.py:227
msgid "error while saving config: %s%s"
msgstr "ошибка во время сохранения настроек: %s%s"

#: options.py:151
msgid "Error during on_import - option: %s."
msgstr "Ошибка"

#: options.py:233
msgid "GnomeKeyring is not available!!"
msgstr "GnomeKeyring не доступен"

#: options.py:239
msgid "No keyrings found. Please create one first!"
msgstr ""

#: options.py:245
msgid "Warning: No default keyring found, storage is not permanent!"
msgstr ""

#: options.py:261
msgid "ERROR: Unable to read password from keyring: %s"
msgstr ""

#: options.py:266
msgid "Illegal value in AccountOption.on_import."
msgstr "Некорректное значение в AccountOption.on_import."

#: options.py:394
msgid "Options: Error - group %s not defined."
msgstr "Настройки: Ошибка - группа %s  не определена."

#: options.py:452
msgid "Invalid XML in metadata-file (or file missing): \"%s\"."
msgstr "Некорректный XML в файле метаданных (или файл отсутствует): \"%s\"."

#: options.py:456
msgid "Missing or invalid rootnode in metadata-file: \"%s\"."
msgstr ""

#: options.py:465
msgid ""
"Error in metadata-file \"%s\" - only <group>-tags allowed in first level. "
"Groups must contain at least one <info>-element."
msgstr ""

#: options.py:471
msgid "No name for group defined in \"%s\"."
msgstr ""

#: options.py:487
msgid "Invalid option-node found in \"%s\"."
msgstr ""

#: options.py:603
msgid "PRESS: %s"
msgstr "НАЖМИТЕ: %s"

#: options.py:640
msgid "Edit Options"
msgstr "Изменить Настройки"

#: options.py:770
msgid "Path %s not found."
msgstr "Путь %s не найден."

#: options.py:806
msgid "active theme is: %s"
msgstr "текущая тема: %s"

#: options.py:836
msgid "About "
msgstr "О "

#: options.py:848
msgid "Options "
msgstr "Настройки "

#: options.py:855
msgid ""
"Themes allow you to easily switch the appearance of your Screenlets. On this "
"page you find a list of all available themes for this Screenlet."
msgstr ""

#: options.py:884
msgid "Themes "
msgstr "Темы "

#: options.py:896
msgid "no info available"
msgstr "информация недоступна"

#: options.py:983
msgid "Choose File"
msgstr "Выберите файл"

#: options.py:992
msgid "Choose Directory"
msgstr "Выберите директорию"

#: options.py:1018
msgid "Choose Image"
msgstr "Выберите изображение"

#: options.py:1085
msgid "Open List-Editor ..."
msgstr "Открыть редактор списков ..."

#: options.py:1106
msgid "Apply username/password ..."
msgstr "Применить имя/пароль ..."

#: options.py:1107
msgid "Enter username here ..."
msgstr "Введите имя пользователя ..."

#: options.py:1108
msgid "Enter password here ..."
msgstr "Введите пароль ..."

#: options.py:1146
msgid "unsupported type ''"
msgstr "не поддерживаемый тип ''"

#: options.py:1163
msgid "Apply"
msgstr "Применить"

#: options.py:1224
msgid "OptionsDialog: Unknown option type: %s"
msgstr ""

#: options.py:1234
msgid "Changed: %s"
msgstr "Изменено: %s"

#: sensors.py:52
#: sensors.py:90
#: sensors.py:105
msgid "Failed to open /proc/stat"
msgstr "Невозможно открыть /proc/stat"

#: sensors.py:76
msgid "Failed to open /proc/cpuinfo"
msgstr "Невозможно открыть /proc/cpuinfo"

#: sensors.py:130
msgid "%s days, %s hours and %s minutes"
msgstr "%s дней, %s часов и %s минут"

#: sensors.py:146
msgid "Failed to open /proc/uptime"
msgstr "Невозможно открыть /proc/uptime"

#: sensors.py:205
msgid "Error getting distro name"
msgstr "Ошибка в получении названия дистрибутива"

#: sensors.py:234
msgid "Can't get kernel version"
msgstr "Невозможно получить версию ядра"

#: sensors.py:243
msgid "Can't get KDE version"
msgstr "Невозможно получить версию KDE"

#: sensors.py:252
msgid "Can't get Gnome version"
msgstr "Невозможно получить версию Gnome"

#: sensors.py:322
msgid "Can't parse /proc/meminfo"
msgstr "Не возможно проанализировать /proc/meminfo"

#: sensors.py:456
msgid "Cannot get ip"
msgstr "Невозможно получить IP"

#: sensors.py:475
msgid "Can't open /proc/net/dev"
msgstr "Невозможно открыть /proc/net/dev"

#: sensors.py:511
msgid "Can't open /proc/net/wireless"
msgstr "Невозможно открыть /proc/net/wireless"

#: sensors.py:523
#: sensors.py:536
msgid "Not connected"
msgstr "Не подключено"

#: sensors.py:786
msgid "Can't open %s/temperature"
msgstr "Невозможно открыть %s/temperature"

#: sensors.py:788
msgid "Can't open folder /proc/acpi/thermal_zone/"
msgstr "Невозможно открыть папку /proc/acpi/thermal_zone/"

#: sensors.py:804
#: sensors.py:815
msgid "Can't open %s"
msgstr "Невозможно открыть %s"

#: sensors.py:830
msgid "Error during hddtemp drives search"
msgstr ""

#: sensors.py:832
#: sensors.py:909
msgid "Hddtemp not installed"
msgstr "Hddtemp не установлен"

#: sensors.py:862
msgid "can't read temperature in: %s"
msgstr "невозможно прочесть температуру для: %s"

#: sensors.py:876
#: sensors.py:892
msgid "Can't read value from %s"
msgstr "Невозможно получить значение из %s"

#: sensors.py:1030
msgid "CPUSensor: Failed to open /proc/stat. Sensor stopped."
msgstr "CPUSensor: Невозможно открыть /proc/stat. Сенсор остановлен."

#: services.py:54
msgid "No name set in ScreenletService.__init__!"
msgstr ""

#: services.py:68
msgid "TEST: %s"
msgstr "ТЕСТ: %s"

#: services.py:73
msgid "DEBUG: %s"
msgstr "Отладка: %s"

#: services.py:103
#: services.py:143
msgid "Cannot get/set protected options through service."
msgstr ""

#: services.py:135
msgid "Trying to access invalid instance \"%s\"."
msgstr ""

#: services.py:137
msgid "Trying to access invalid option \"%s\"."
msgstr ""

#: services.py:169
msgid "Error in screenlets.services.get_service_by_name: %s"
msgstr ""

#: session.py:72
msgid ""
"ScreenletSession.__init__ has to be called with a\n"
"\t\t\t valid Screenlet-classobject as first argument!"
msgstr ""

#: session.py:94
msgid "Unable to init backend - settings will not be saved!"
msgstr ""

#: session.py:112
msgid "Error in screenlets.session.connect_daemon: %s"
msgstr "Ошибка в screenlets.session.connect_daemon: %s"

#: session.py:117
msgid "Creating new instance: "
msgstr ""

#: session.py:120
msgid "ID is unset or already in use - creating new one!"
msgstr ""

#: session.py:126
#: session.py:376
msgid "File: %s"
msgstr "Файл: %s"

#: session.py:132
#: session.py:382
msgid "Set options in %s"
msgstr ""

#: session.py:159
msgid "Failed to remove INI-file for instance (not critical)."
msgstr ""

#: session.py:163
msgid "Removing last instance from session"
msgstr ""

#: session.py:165
msgid "TODO: remove self.path: %s"
msgstr ""

#: session.py:169
msgid "Failed to remove session dir '%s' - not empty?"
msgstr ""

#: session.py:174
#: session.py:201
msgid "Removing instance from session but staying alive"
msgstr ""

#: session.py:223
msgid "Found a running session of %s, adding new instance by service."
msgstr ""

#: session.py:226
msgid "Adding new instance through: %s"
msgstr ""

#: session.py:232
msgid "Loading instances in: %s"
msgstr ""

#: session.py:235
msgid "Restored instances from session '%s' ..."
msgstr ""

#: session.py:241
msgid "No instance(s) found in session-path, creating new one."
msgstr ""

#: session.py:255
msgid "Failed creating instance of: %s"
msgstr ""

#: session.py:273
msgid ""
"Error: Unable to create temp entry - screenlets-manager will not work "
"properly."
msgstr ""

#: session.py:281
msgid "Creating new entry for %s in %s"
msgstr ""

#: session.py:294
msgid "No global tempfile found, creating new one."
msgstr ""

#: session.py:297
msgid ""
"Error: Unable to create temp directory %s - screenlets-manager will not work "
"properly."
msgstr ""

#: session.py:316
msgid "Failed to unregister from daemon: %s"
msgstr ""

#: session.py:321
msgid "Removing entry for %s from global tempfile %s"
msgstr ""

#: session.py:326
msgid "Entry not found. Will (obviously) not be removed."
msgstr ""

#: session.py:338
msgid "Error global tempfile not found. Some error before?"
msgstr ""

#: session.py:341
msgid "No more screenlets running."
msgstr "Запущенных Скринлетов больше нет"

#: session.py:344
msgid "No screenlets running?"
msgstr "Нет запущенных Скринлетов?"

#: session.py:350
msgid "Deleting global tempfile %s"
msgstr ""

#: session.py:355
msgid "Error: Failed to delete global tempfile"
msgstr ""

#: session.py:389
msgid "Failed to create instance of '%s'!"
msgstr ""

#: session.py:422
msgid "Screenlet has been killed. TODO: make this an event"
msgstr ""

#: session.py:433
msgid ""
"Screenlet '%s' has been interrupted by keyboard. TODO: make this an event"
msgstr ""

#: session.py:435
msgid "Exception in ScreenletSession: "
msgstr ""

#: utils.py:81
msgid "Unable to load '%s' from %s: %s "
msgstr "Невозможно загрузить '%s' из %s: %s "

#: utils.py:100
msgid "LISTED PATH NOT EXISTS: "
msgstr ""

#: utils.py:199
msgid "File %s not found"
msgstr "Файл %s не найден"

#: utils.py:230
msgid "Section %s not found!"
msgstr "Секция %s не найдена!"

#: utils.py:264
msgid "Notify: No DBus running or notifications-daemon unavailable."
msgstr ""
